import { Component, OnInit, Input, Output } from '@angular/core';
import { UsuarioEnt } from 'src/app/entidades/usuarioEnt';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.sass']
})
export class FormularioComponent{

  @Input() usuarioNuevo : UsuarioEnt;
  @Output() propagarBlur = new EventEmitter<string>();
  @Output() propagarClass = new EventEmitter<string>();
  @Output() propagarClass1 = new EventEmitter<string>();
  @Output() propagarClass2 = new EventEmitter<string>();
  @Output() propagarClass3 = new EventEmitter<string>();

alPerderFocoEmail() {
  
  this.propagarBlur.emit("Mensaje");
}

colorEmail(){
  this.propagarClass.emit("Mensaje");
}

tickEmail(){
  this.propagarClass1.emit("Correcto");
}

cruzEmail(){
  this.propagarClass2.emit("Mensaje");
}
parrafoEmail(){
  this.propagarClass3.emit("Mensaje");
}

}
