import { Component, OnInit } from '@angular/core';
import { UsuarioEnt } from "../../entidades/usuarioEnt";
import { HttpService } from "../../servicios/http.service";
import { isEmptyExpression } from '@angular/compiler';
import { dashCaseToCamelCase } from '@angular/compiler/src/util';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.sass']
})
export class RegistroComponent /*implements OnInit*/ {
  usuarioNuevo: UsuarioEnt;
  camposInvalid = false;
  objetable;
  valido: String;
  
  constructor(private infUsu: HttpService ) {
    this.usuarioNuevo = new UsuarioEnt();
    this.usuarioNuevo.nombre = "";
    this.usuarioNuevo.apellidos = "";
    this.usuarioNuevo.edad;
    this.usuarioNuevo.email = "";
    this.usuarioNuevo.telefono = "";
    this.usuarioNuevo.dni = "";
    this.usuarioNuevo.ciudad = "";
    this.usuarioNuevo.cp;
    this.usuarioNuevo.password = "";

   }

   /**
    * Función que comprueba que ciertos campos no estén vacíos. Si están vacíos no permite
    * registrar el usuario. Si está rellenos los campos inserta en la base de datos.
    */
  registroComponentClick(){

    if(this.usuarioNuevo.nombre === "" || this.usuarioNuevo.apellidos === "" ||
      this.usuarioNuevo.edad === 0 || this.usuarioNuevo.email === "" ||
      this.usuarioNuevo.telefono === "" || this.usuarioNuevo.dni === "" || this.usuarioNuevo.password === ""){
      alert("Hay campos obligatorios");
    }else{
      this.infUsu.insertarUsuariosBD(this.usuarioNuevo);
      this.usuarioNuevo = new UsuarioEnt();
    }


  }

  /**
   * Cuando el usuario escribe el email en el input se le muestra si ya existe o no existe.
   */
  alPerderFocoEmail() {
    if(this.usuarioNuevo.email === ""){

    }else{
      this.objetable = this.infUsu.existeEmail(this.usuarioNuevo);

      this.objetable.subscribe(datos=>{
        if(datos.valido === true){
          // const correcto = document.getElementById("registro-email")
          // correcto.setAttribute("style", "border: 1px solid green")
        this.valido = "Incorrecto"
        
        }else{
          this.valido = "Correcto"
          // const incorrecto = document.getElementById("registro-email")
          // incorrecto.setAttribute("style", "border: 1px solid red")
        }
      })
    }
 }

 colorEmail(){
    
  if(this.valido === "Correcto" ){
    return "registro__email-correcto"
  }else if(this.valido === "Incorrecto"){
    return "registro__email-incorrecto"
  }
}
parrafoEmail(){
  if(this.valido === "Incorrecto"){
    return "registro__email-incorrecto-p"
  }else{
    return "registro__email-incorrecto-oculto"
  }
}
tickEmail(clase){
  if(this.valido === "Correcto"){
    return "registro__email-tick-correcto"
  }else{
    return "registro__email-tick-oculto"
  }
}
cruzEmail(){
  if(this.valido === "Incorrecto"){
    return "registro__email-tick-correcto"
  }else{
    return "registro__email-tick-oculto"
  }
}

  ngOnInit() {
  }

}
